#include <stdio.h>

#include <core_i2c.h>
#include <led_strip.h>
#include <vl53l0x_api.h>
#include <driver/rmt.h>

#include "util.h"

#define NUM_LEDS 16
#define RANGING_PERIOD_MS 200
#define BAR_UPDATE_DELAY_MS 100
#define RANGE_RESOLUTION_MM 60
#define RANGE_MAX_MM (RANGE_RESOLUTION_MM * NUM_LEDS)
#define LED_BAR_GPIO 25
#define ATOM_BUTTON_GPIO 39
#define INTENSITY_LOWEST 5

union led_color {
	struct {
		uint8_t r;
		uint8_t g;
		uint8_t b;
	};
	uint32_t color;
};

static const union led_color colors[NUM_LEDS] = {
	{.r =   0, .g = 255},
	{.r =   1, .g = 254},
	{.r =   4, .g = 254},
	{.r =   8, .g = 254},
	{.r =  20, .g = 254},
	{.r =  39, .g = 251},
	{.r =  85, .g = 240},
	{.r = 143, .g = 210},
	{.r = 210, .g = 143},
	{.r = 240, .g =  85},
	{.r = 251, .g =  39},
	{.r = 254, .g =  20},
	{.r = 254, .g =   8},
	{.r = 254, .g =   4},
	{.r = 254, .g =   1},
	{.r = 255, .g =   0}
};

static led_strip_t *bar;
static VL53L0X_Dev_t tof = {
	.i2c_address = 0x29,
	.i2c_port_num = 0,
};
static bool tof_ok = false;
// Number of shifts, the higher, the less bright are the LEDs
static uint_fast8_t intensity = 3;

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
	intensity++;

	if (intensity > INTENSITY_LOWEST) {
		intensity = 0;
	}
}

static void led_bar_setup(void)
{
	rmt_config_t config = RMT_DEFAULT_CONFIG_TX(LED_BAR_GPIO, RMT_CHANNEL_0);
	// Set counter clock to 40MHz
	config.clk_div = 2;

	ESP_ERROR_CHECK(rmt_config(&config));
	ESP_ERROR_CHECK(rmt_driver_install(config.channel, 0, 0));

	// install ws2812 driver
	led_strip_config_t strip_config = LED_STRIP_DEFAULT_CONFIG(NUM_LEDS,
			(led_strip_dev_t)config.channel);
	bar = led_strip_new_rmt_ws2812(&strip_config);
	if (!bar) {
		LOGE("install WS2812 driver failed");
	}
	// Turn off LEDs
	ESP_ERROR_CHECK(bar->clear(bar, 100));
}

int16_t tof_measure(void)
{
	VL53L0X_Error status;
	VL53L0X_RangingMeasurementData_t data;

	if (!tof_ok) {
		return -1;
	};

	// Get measurement from previous cycle
	status = VL53L0X_GetRangingMeasurementData(&tof, &data);
	if (VL53L0X_ERROR_NONE != status) {
		LOGW("measurement read failed, err %d", status);
		return -1;
	};

	// Start new measurement that will read in next iteration
	status = VL53L0X_StartMeasurement(&tof);
	if (VL53L0X_ERROR_NONE != status) {
		LOGW("measurement start failed, err %d", status);
		tof_ok = false;
		return -1;
	};

	// Only return measurement if range is valid
	if (0 != data.RangeStatus) {
		return -1;
	}

	return data.RangeMilliMeter;
}

static void bar_update(uint32_t range)
{
	const uint32_t on_leds = NUM_LEDS - (range / RANGE_RESOLUTION_MM);
	const union led_color color = colors[on_leds - 1];
	uint32_t i;

	for (i = 0; i < on_leds; i++) {
		bar->set_pixel(bar, i, color.r>>intensity, color.g>>intensity,
				color.b>>intensity);
	}

	for (; i < NUM_LEDS; i++) {
		bar->set_pixel(bar, i, 0, 0, 0);
	}

	bar->refresh(bar, BAR_UPDATE_DELAY_MS);
}

static void ranging_tsk(void *arg)
{
	int32_t range;

	while (true) {
		if ((range = tof_measure()) >= 0 && range < RANGE_MAX_MM) {
			LOGD("range mm: %d", range);
			bar_update(range);
		} else {
			bar->clear(bar, BAR_UPDATE_DELAY_MS);
		}
		vTaskDelay(pdMS_TO_TICKS(RANGING_PERIOD_MS));
	}
}

static int tof_setup(void)
{
	VL53L0X_Error status;
	VL53L0X_DeviceInfo_t info = {};
	uint32_t spad_n;
	uint8_t aperture;

	status = VL53L0X_GetDeviceInfo(&tof, &info);
	if (VL53L0X_ERROR_NONE != status) {
		LOGW("VL53L0x device not found, err %d", status);
		return -1;
	};
	LOGI("%s:%s:%s v%d.%d", info.Type, info.Name, info.ProductId,
			info.ProductRevisionMajor, info.ProductRevisionMinor);
    
    	status = VL53L0X_DataInit(&tof);
	if (VL53L0X_ERROR_NONE != status) {
		LOGW("VL53L0x data init failed, err %d", status);
		return -1;
	};

	status = VL53L0X_StaticInit(&tof);
	if (VL53L0X_ERROR_NONE != status) {
		LOGW("VL53L0x static init failed, err %d", status);
		return -1;
	};

	status = VL53L0X_PerformRefSpadManagement(&tof, &spad_n, &aperture);
	if (VL53L0X_ERROR_NONE != status) {
		LOGW("VL53L0x spads init failed, err %d", status);
		return -1;
	};
	LOGD("spads: %d, aperture: %d", spad_n, aperture);

	status = VL53L0X_SetDeviceMode(&tof, VL53L0X_DEVICEMODE_SINGLE_RANGING);
	if (VL53L0X_ERROR_NONE != status) {
		LOGW("VL53L0x set polled mode failed, err %d", status);
		return -1;
	};

	tof_ok = true;

	return 0;
}

static void button_cfg(int32_t gpio_pin)
{
	const gpio_config_t io_conf = {
		.pin_bit_mask = ((uint64_t)1)<<gpio_pin,
		.mode = GPIO_MODE_INPUT,
		.intr_type = GPIO_INTR_NEGEDGE,
		.pull_down_en = 0,
		.pull_up_en = 1
	};

	//configure GPIO with the given settings
	gpio_config(&io_conf);
	//install gpio isr service
	gpio_install_isr_service(0);
	gpio_isr_handler_add(gpio_pin, gpio_isr_handler, (void*)gpio_pin);
}

void app_main(void)
{
	ESP_ERROR_CHECK(ci2c_init());
	led_bar_setup();
	button_cfg(ATOM_BUTTON_GPIO);
	tof_setup();

	xTaskCreate(ranging_tsk, "gui_test", 4096, NULL, tskIDLE_PRIORITY + 1, NULL);
}
