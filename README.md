# Introduction

This project uses a VL53L0X sensor to drive 16 "neopixel" LEDs according to the detected distance. It uses a ESP32 MCU and builds against [esp-idf](https://github.com/espressif/esp-idf).
