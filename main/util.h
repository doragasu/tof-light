#ifndef __UTIL_H__
#define __UTIL_H__

#include <esp_log.h>

#define UNUSED __attribute__((unused))

#define SATURATE(val, low, high) ((val)<(low)?(low):(val)>(high)?(high):(val))

#define LOGE(...) ESP_LOGE(__func__, __VA_ARGS__)
#define LOGD(...) ESP_LOGD(__func__, __VA_ARGS__)
#define LOGI(...) ESP_LOGI(__func__, __VA_ARGS__)
#define LOGW(...) ESP_LOGW(__func__, __VA_ARGS__)

#endif
